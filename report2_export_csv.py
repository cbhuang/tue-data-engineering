"""
Export CSV files for Report 2

Usage: Open Neo4J DB and then `python3 report2_export_csv.py`
Note: In IPython, please reset absolute path for PATH_OUT
"""

import neo4j
import os
from settings import *

PATH_OUT = "TEMP"

#driver = neo4j.GraphDatabase.driver(uri, auth=("neo4j", "team6"))

class Neo4J_Parser():

    def __init__(self):
        self.NEO4J_HOST = NEO4J_HOST
        self.NEO4J_PORT = NEO4J_PORT
        self.NEO4J_USER = NEO4J_USER
        self.NEO4J_PASSWD = NEO4J_PASSWD
        self.driver = neo4j.GraphDatabase.driver(f"bolt://{NEO4J_HOST}:{NEO4J_PORT}",
                                                 auth=(NEO4J_USER, NEO4J_PASSWD))
        self.session = self.driver.session()
        self.PATH_OUT = PATH_OUT

    def __del__(self):
        self.session.close()

    def export_users(self):
        query = """
            MATCH (n:users)
            RETURN 
                n.UserId as UserId,
                n.DisplayName as DisplayName, 
                toInteger(n.Reputation) as Reputation, 
                toInteger(n.UpVotes) as UpVotes, 
                toInteger(n.DownVotes) as DownVotes, 
                toInteger(n.Views) as Views, 
                toFloat(n.Age) as Age,
                n.CreationDate as CreationDate, 
                n.LastAccessDate as LastAccessDate;"""
        result = self.session.run(query)
        # format output
        ls_lines = []
        for rec in result:
            # note: use {rec["property"] or ''} to avoid "None"
            s = f"""{rec['UserId']},"Node Label","users","DisplayName","{rec['DisplayName'] or ''}","Reputation",{rec['Reputation'] or ''},"UpVotes",{rec['UpVotes'] or ''},"DownVotes",{rec['DownVotes'] or ''},"Views",{rec['Views'] or ''},"Age",{rec['Age'] or ''},"CreationDate","{rec['CreationDate'] or ''}","LastAccessDate","{rec['LastAccessDate'] or ''}"\n"""
            ls_lines.append(s)
        # write file
        fname = os.path.join(PATH_OUT, "nodes_users.csv")
        with open(fname, "w+") as f:
            f.writelines(ls_lines)

    def export_posts(self):
        query = """
            MATCH (n:posts)
            RETURN 
                toInteger(n.PostId) as PostId,
                toInteger(n.PostTypeId) as PostTypeId,
                toInteger(n.AcceptedAnswerId) as AcceptedAnswerId,
                n.CreaionDate as CreaionDate,
                toInteger(n.Score) as Score,
                toInteger(n.ViewCount) as ViewCount,
                toInteger(n.OwnerUserId) as OwnerUserId,
                n.LasActivityDate as LasActivityDate,
                n.Title as Title,
                n.Tags as Tags,
                toInteger(n.AnswerCount) as AnswerCount,
                toInteger(n.CommentCount) as CommentCount,
                toInteger(n.FavoriteCount) as FavoriteCount,
                toInteger(n.LastEditorUserId) as LastEditorUserId,
                n.LastEditDate as LastEditDate,
                toInteger(n.ParentId) as ParentId,
                n.ClosedDate as ClosedDate;"""
        result = self.session.run(query)
        # format output
        ls_lines = []
        for rec in result:
            s = f"""{rec['PostId']},"Node Label","posts","PostTypeId","{rec['PostTypeId'] or ''}","ParentId","{rec['ParentId'] or ''}","AcceptedAnswerId",{rec['AcceptedAnswerId'] or ''},"OwnerUserId","{rec['OwnerUserId'] or ''}","Score",{rec['Score'] or ''},"ViewCount",{rec['ViewCount'] or ''},"AnswerCount",{rec['AnswerCount'] or ''},"CommentCount",{rec['CommentCount'] or ''},"FavoriteCount","{rec['FavoriteCount'] or ''}","LastEditorUserId","{rec['LastEditorUserId'] or ''}","Title","{rec['Title'] or ''}","Tags","{rec['Tags'] or ''}","CreaionDate","{rec['CreaionDate'] or ''}","LastEditDate","{rec['LastEditDate'] or ''}","LasActivityDate","{rec['LasActivityDate'] or ''}","ClosedDate","{rec['ClosedDate'] or ''}"\n"""
            ls_lines.append(s)
        # write file
        fname = os.path.join(PATH_OUT, "nodes_posts.csv")
        with open(fname, "w+") as f:
            f.writelines(ls_lines)

    def export_users_by_same_question(self):
        query = """
            MATCH (u1:users)-[r:SAME_QUESTION]->(u2:users)
                WHERE u1.UserId < u2.UserId
            return 
                u1.UserId as FromNodeID, 
                u2.UserId as ToNodeID, 
                type(r) as EdgeLabel
            order by FromNodeID, ToNodeID, EdgeLabel"""
        result = self.session.run(query)
        # format output
        ls_lines = []
        for i, rec in enumerate(result):
            # note: EdgeID is not in the network, hence given here as i
            s = f"""{i},{rec['FromNodeID']},{rec['ToNodeID']},"Edge Label","{rec['EdgeLabel'] or ''}"\n"""
            ls_lines.append(s)
        # write file
        fname = os.path.join(PATH_OUT, "edges_users_by_same_question.csv")
        with open(fname, "w+") as f:
            f.writelines(ls_lines)

    def export_users_by_answers(self):
        query = """
            MATCH (u1:users)-[r:ANSWERS]-(u2:users)
                WHERE u1.UserId <= u2.UserId
            return 
                u1.UserId as FromNodeID, 
                u2.UserId as ToNodeID, 
                type(r) as EdgeLabel
            order by FromNodeID, ToNodeID, EdgeLabel"""
        result = self.session.run(query)
        # format output
        ls_lines = []
        for i, rec in enumerate(result):
            # note: EdgeID is not in the network, hence given here as i
            s = f"""{i},{rec['FromNodeID']},{rec['ToNodeID']},"Edge Label","{rec['EdgeLabel'] or ''}"\n"""
            ls_lines.append(s)
        # write file
        fname = os.path.join(PATH_OUT, "edges_users_by_answers.csv")
        with open(fname, "w+") as f:
            f.writelines(ls_lines)

    def export_users_by_same_thread(self):
        query = """
            MATCH (u1:users)-[r:SAME_THREAD]->(u2:users)
            return 
                u1.UserId as FromNodeID, 
                u2.UserId as ToNodeID, 
                type(r) as EdgeLabel
            order by FromNodeID, ToNodeID, EdgeLabel"""
        result = self.session.run(query)
        # format output
        ls_lines = []
        for i, rec in enumerate(result):
            # note: EdgeID is not in the network, hence given here as i
            s = f"""{i},{rec['FromNodeID']},{rec['ToNodeID']},"Edge Label","{rec['EdgeLabel'] or ''}"\n"""
            ls_lines.append(s)
        # write file
        fname = os.path.join(PATH_OUT, "edges_users_by_same_thread.csv")
        with open(fname, "w+") as f:
            f.writelines(ls_lines)

    def export_users_by_same_thread_multi(self):
        """Export edges by SAME_THREAD with multiplicity and detailed info """
        
        query = """
            MATCH (u1:users)-[r:SAME_QUESTION|:ANSWERS]->(u2:users)
            return 
                u1.UserId as FromNodeID, 
                u2.UserId as ToNodeID, 
                type(r) as EdgeLabel,
                r.QuestionId as QuestionId
            order by FromNodeID, ToNodeID, EdgeLabel, QuestionId"""
        result = self.session.run(query)
        # format output
        ls_lines = []
        for i, rec in enumerate(result):
            # note: EdgeID is not in the network, hence given here as i
            s = f"""{i},{rec['FromNodeID']},{rec['ToNodeID']},"Edge Label","{rec['EdgeLabel'] or ''}","QuestionId","{rec['QuestionId'] or ''}"\n"""
            ls_lines.append(s)
        # write file
        fname = os.path.join(PATH_OUT, "edges_users_by_same_thread_multi.csv")
        with open(fname, "w+") as f:
            f.writelines(ls_lines)

    def export_edges_posts_by_same_tag(self):
        """CAUTION: 1. direction! 2. only some selected tags are used!"""
        query = """
                MATCH (post1)-[r:TMP_SAME_TAG]-(post2)
                return 
                    post1.PostId as FromNodeID, 
                    post2.PostId as ToNodeID, 
                    type(r) as EdgeLabel;"""
        result = self.session.run(query)
        # format output
        ls_lines = []
        for i, rec in enumerate(result):
            # note: EdgeID is not in the network, hence given here as i
            s = f"""{i},{rec['FromNodeID']},{rec['ToNodeID']},"Edge Label","{rec['EdgeLabel'] or ''}"\n"""
            ls_lines.append(s)
        # write file
        fname = os.path.join(PATH_OUT, "edges_posts_by_same_tag.csv")
        with open(fname, "w+") as f:
            f.writelines(ls_lines)

    def export_edges_posts_by_same_answerer(self):
        """CAUTION: only some selected tags are used!"""
        query = """
                MATCH (post1)-[r:SAME_ANSWERER]->(post2)
                WHERE post1.PostId < post2.PostId
                return 
                    post1.PostId as FromNodeID, 
                    post2.PostId as ToNodeID, 
                    type(r) as EdgeLabel;"""
        result = self.session.run(query)
        # format output
        ls_lines = []
        for i, rec in enumerate(result):
            # note: EdgeID is not in the network, hence given here as i
            s = f"""{i},{rec['FromNodeID']},{rec['ToNodeID']},"Edge Label","{rec['EdgeLabel'] or ''}"\n"""
            ls_lines.append(s)
        # write file
        fname = os.path.join(PATH_OUT, "edges_posts_by_same_answerer.csv")
        with open(fname, "w+") as f:
            f.writelines(ls_lines)

if __name__ == '__main__':
    a = Neo4J_Parser()
    # User Network
    a.export_users()
    a.export_users_by_answers()
    a.export_users_by_same_question()
    a.export_users_by_same_thread()  # no multiplicity
    a.export_users_by_same_thread_multi()  # retains multiplicity (future use)
    # Post Network
    a.export_posts()
    a.export_edges_posts_by_same_answerer()
    a.export_edges_posts_by_same_tag()  # exploratory (future use)
    del a
    # TODO: (optional) add "tag" property to :TMP_SAME_TAGS
    
