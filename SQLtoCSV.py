"""
Export raw data to csv (for neo4j DB to import)

Usage: python3 SQLtoCSV.py
"""
import os
import pandas as pd
import pymysql
from settings import *

# file output path
out_path = f"{os.getcwd()}/data"

# preprocessed database
conn = pymysql.connect(host=HOST,
                       user=USER,
                       password=PASSWD,
                       db=DB_RAW,
                       cursorclass=pymysql.cursors.DictCursor)
cur = conn.cursor()
# don't forget to close both!

# list of size of tables
sql = """select TABLE_NAME, TABLE_ROWS
         from INFORMATION_SCHEMA.TABLES
         where TABLE_SCHEMA = "stackexchange";"""
cur.execute(sql)
fetched = cur.fetchall()
df_rows = pd.DataFrame.from_dict(fetched)
# df_rows.to_csv("result/db_tables_rows.csv")
print(df_rows)

# export each table
for tablename in df_rows['TABLE_NAME']:
    sql = f"select * from {tablename};"
    cur.execute(sql)
    fetched = cur.fetchall()
    df = pd.DataFrame.from_dict(fetched)
    df.to_csv(f"{out_path}/{tablename}.csv", index=False) #na_rep="string value"

#to show how pandas read and save the NaN value.
# sql = """select * from tags;"""
# cur.execute(sql)
# fetched = cur.fetchall()
# df_rows = pd.DataFrame.from_dict(fetched)
# print(df_rows.head())
# print(df_rows.loc[2,'ExcerptPostId'])
# print(type(df_rows.loc[2,'ExcerptPostId']))

# cleanup
cur.close()
conn.close()

