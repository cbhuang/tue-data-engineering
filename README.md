# tue-data-engineering

TU/e Data Engineering (2DD23) Team6 workspace

## Project Structure

Main:

- `/data`: raw data
- `/sql`: SQL code for data preprocession
- `/result`: files for report writing

Others:

- `/changelog`: commit messages
- `/img`: screenshots and images to show
- `/TEMP`: Your personal working directory. They are set to NOT be pushed to GitHub in `.gitignore`.

## Summary Information

### 1. User

The network is Users connected by common Post and Comments

- Number of nodes: 22558
- Number of edges: 152314
- Average degree:  13.5042

### 2. Post

The network is Posts connected by common Users.

- Number of nodes: 43484
- Number of edges: 14012156
- Average degree:  644.474
