UNWIND [0,2,1,3] as L
MATCH (u:users {Role: L})
RETURN L as Role, count(u) as N, 
    avg( size( (u)-[:SAME_THREAD]-() ) ) as avg_degree,
    avg(toInteger(u.Reputation)) as avg_Reputation,
    avg(toInteger(u.UserId)) as avg_UserId,
    avg(toInteger(u.Age)) as avg_Age,
    avg(toInteger(u.Views)) as avg_Views,
    avg(toInteger(u.UpVotes)) as avg_UpVotes,
    sum(toFloat(u.DownVotes)) / sum(toFloat(u.UpVotes) + toFloat(u.DownVotes)) as avg_DownVoteRatio,
    avg(size(u.DisplayName)) as avg_len_DisplayName,
    avg(size(u.AboutMe)) as avg_len_AboutMe;

