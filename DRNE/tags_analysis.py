import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os

base_dir = "/mnt/data/GitLab/tue-data-engineering/DRNE"
tags_dir = os.path.join(base_dir, "tags")
infile_path = os.path.join(tags_dir, "user_role_tags.csv")

# r is too much -> deleted!
select_lang = False
lang_list = ["python", "c", "c++", "sas", "assembly", "fortran", "java", "javascript", "bash", "shell", "ruby", "html", "css", "php"]
n_top = 15


df = pd.read_csv(infile_path)

# select elements to keep
if select_lang:
    ls_select = np.zeros(len(df), dtype="bool")
    for i in range(len(df)):
        if df["TagName"].iloc[i] in lang_list:
            ls_select[i] = True
        else:
            ls_select[i] = False
    df = df[["Role", "TagName"]].iloc[ls_select]


fig = plt.figure(figsize=(16, 12))
for role in range(4):
    # filter elements
    df_role = df[df["Role"] == role]
    # count
    df_role = df_role["TagName"].value_counts()
    # pick bars
    df_role = df_role[:n_top]  # first 10 bins
    # plot
    ax = fig.add_subplot(2,2, role+1)
    df_role.plot(ax=ax, kind="bar", subplots=True)
    ax.set_title(f"Role={role}", fontsize=20)
    ax.tick_params(axis="x", labelsize=14)
    ax.tick_params(axis="y", labelsize=14)

fig.show()
fig.savefig(os.path.join(tags_dir, f"hist_top={n_top}.png"))
plt.close(fig)
