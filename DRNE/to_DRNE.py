import os
import subprocess 
from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import collections
from neo4j import GraphDatabase

"""
Parameters
=============
"""
# path
py3 = "/opt/anaconda3/bin/python3"
base_dir = "/mnt/data/GitLab/tue-data-engineering/DRNE"
infile_path = os.path.join(base_dir, "in/edges_users_by_same_thread.csv")
out_dir = os.path.join(base_dir, "out")
edgelist_path = os.path.join(out_dir, "users.edgelist")
save_path = os.path.join(out_dir, "users")

# model
#ls_embedding_size = [4, 8, 16, 64]
ls_embedding_size = [16]
batch_size = 256  # default: 256
learning_rate = 0.0005  # default: 0.0025
epochs = 20  # default: 20

"""
Renumber Nodes
================

Renumber to consecutive and 0-based labels!
"""

# Load
ls_from = []
ls_to = []
with open(infile_path) as f_in:
    for line in f_in:
        tokens = line.split(",")
        ls_from.append(int(tokens[1]))
        ls_to.append(int(tokens[2]))

# numbering
ls_nodes = np.sort(list(set(ls_from + ls_to)))  # sort distinct
print(f"[Info] There are {len(ls_nodes)} distinct nodes!")  # 16,676
dic_node_encode = dict()  # neo4j to DRNE
dic_node_decode = dict()  # DRNE to neo4j
for i, node in enumerate(ls_nodes):
    dic_node_encode[node] = i  # 0-based
    dic_node_decode[i] = node

# output
with open(edgelist_path, "w+") as f_out:  # users.edgelist
    for i in range(len(ls_from)):
        new_from = dic_node_encode[ls_from[i]]
        new_to = dic_node_encode[ls_to[i]]
        f_out.write(f"{new_from} {new_to}\n")

# execute
for embedding_size in ls_embedding_size:
    cmd = [
        py3,
        os.path.join(base_dir, "src/main.py"),
        "--data_path",
        edgelist_path,
        "--save_path",
        save_path,
        "--save_suffix",
        "test",
        "-e",
        str(epochs),
        "-s",
        str(embedding_size),  # embedding size
        "-b",
        str(batch_size),
        "-lr",
        str(learning_rate),
        "--index_from_0",
        "True",
    ]
    ret = subprocess.run(cmd)
    print(f"[Info] DSNE finished! Return code: {ret.returncode}")


"""
Clustering (K-means)
========================

Renumber to consecutive and 1-based labels!
"""

# 1. determine k

for embedding_size in ls_embedding_size:

    # load embedding file
    embd_path = f"{out_dir}/users/test_{embedding_size}_0.0_0.5/embeddings.npy"
    embd = np.load(embd_path)

    # try k
    print("[Info] K-means: finding k....")
    ls_k = [i for i in range(2, 16)]
    ls_cost = []
    for k in ls_k:
        # k-means
        kmeans = KMeans(n_clusters=k, random_state=6).fit(embd)
        cost = kmeans.inertia_
        ls_cost.append(cost)
        print(f"k={k}, cost={cost}")

    # save figure
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(1,1,1)
    ax.set_title(f"K-means Cost, embedded_dim={embedding_size}", fontsize=18)
    ax.plot(ls_k, ls_cost)
    ax.set_xlabel("k", fontsize=14)
    ax.set_ylabel("cost", fontsize=14)
    fig.tight_layout()
    fig.show()
    fig.savefig(os.path.join(out_dir, f"user_cost_embd={embedding_size}.png"))
    plt.close(fig)


# 2. plot specific k

for embedding_size in ls_embedding_size:

    # re-do k-means
    k = 4
    embd_path = f"{out_dir}/users/test_{embedding_size}_0.0_0.5/embeddings.npy"
    embd = np.load(embd_path)
    kmeans = KMeans(n_clusters=k, random_state=6).fit(embd)
    cost = kmeans.inertia_

    # calculate distribution
    roles = kmeans.labels_
    unique, counts = np.unique(roles, return_counts=True)
    log10_count = np.log10(counts)

    # plot
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_title(f"Distribution of Roles (k={k})", fontsize=18)
    ax.bar(unique, log10_count)  # plot log-scale
    ax.set_xlabel("Role")
    ax.set_ylabel("log_{10}(count)")
    # show label
    for j in range(k):
        plt.text(j, log10_count[j], str(counts[j]))
    fig.tight_layout()
    fig.show()
    fig.savefig(os.path.join(out_dir, f"user_count_embd={embedding_size}_k={k}.png"))
    plt.close(fig)

"""
Save selected model
===============================

Chosen: embedded dimension=16; k=4 or 5
"""

# model parameters
embedding_size = 16
k = 4
embd_path = f"{out_dir}/users/test_{embedding_size}_0.0_0.5/embeddings.npy"
embd = np.load(embd_path)
kmeans = KMeans(n_clusters=k, random_state=6).fit(embd)
roles = kmeans.labels_  # this is the color_map
n = len(roles)  # number of nodes

# all k's, together
with open(f"{out_dir}/users_roles_k={k}_embd={embedding_size}.txt", "w+") as f:
    for i in range(n):
        f.write(f"{dic_node_decode[i]} {roles[i]}\n")

# only the last role ()
# with open(f"{out_dir}/users_k=3of{k}_embd={embedding_size}.txt", "w+") as f:
#     for i in range(n):
#         if roles[i] == 3:
#             f.write(f"{dic_node_decode[i]} {roles[i]}\n")

"""
Write selected model back to neo4j
===========================================
"""
# https://stackoverflow.com/questions/56710065

# prepare input list as a list of dictionary
data = [{"UserId": str(dic_node_decode[i]),
         "Role": str(roles[i])} for i in range(n)]

driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "team6"))

query = """
    UNWIND $list AS roleList 
    MATCH (u:users {UserId: toInteger(roleList.UserId)}) 
    SET u.Role = toInteger(roleList.Role);
"""

with driver.session() as session:
    with session.begin_transaction() as tx:
        tx.run(query, list=data)


"""
Colorize NetworkX 
===========================================
"""

# construct network
G = nx.read_edgelist(edgelist_path)
# colorize
color_map = []
for i in range(n):
    if roles[i] == 0:
        color_map.append("red")
    elif roles[i] == 1:
        color_map.append("green")
    elif roles[i] == 2:
        color_map.append("blue")
    elif roles[i] == 3:
        color_map.append("black")
    else:
        raise RuntimeError(f"roles[i]={roles[i]} not implemented!")

# degree histogram
def weighted_avg_and_std(values, weights):
    """
    Return the weighted average and standard deviation.

    values, weights -- Numpy ndarrays with the same shape.

    https://stackoverflow.com/a/2415343/3218693
    """
    average = np.average(values, weights=weights)
    # Fast and numerically precise:
    variance = np.average((values-average)**2, weights=weights)
    return (average, np.sqrt(variance))


def plot_deg_hist(deg, cnt, role="all"):
    m, s = weighted_avg_and_std(deg, cnt)
    log_cnt = np.log10(cnt)
    #log_deg = np.log10(np.array(deg) + 1)
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(1,1,1)
    ax.set_title(f"Degree Histogram (Role={role})\nCount={sum(cnt)}, Avg={m:.1f}, Stdev={s:.1f}", fontsize=16)
    ax.plot(deg, log_cnt, ".", color='b')
    ax.set_ylabel("log(Count)", fontsize=14)
    ax.set_xlabel("Degree", fontsize=14)
    fig.show()
    fig.savefig(f"{save_path}/hist_role={role}.png")
    plt.close(fig)


# overall
deg_seq = sorted([d for n, d in G.degree()])
deg_cnt = collections.Counter(deg_seq)
deg, cnt = zip(*deg_cnt.items())
plot_deg_hist(deg, cnt, role="all")

# roles 0~3
for i in range(k):
    deg_seq = sorted([d for n, d in G.degree() if roles[int(n)] == i])
    deg_cnt = collections.Counter(deg_seq)
    deg, cnt = zip(*deg_cnt.items())
    plot_deg_hist(deg, cnt, role=str(i))


# all-in-one (poster)
fig = plt.figure(figsize=(12, 10))
for role in range(k):
    deg_seq = sorted([d for n, d in G.degree() if roles[int(n)] == role])
    deg_cnt = collections.Counter(deg_seq)
    deg, cnt = zip(*deg_cnt.items())

    m, s = weighted_avg_and_std(deg, cnt)
    if role == 0:
        y = np.log10(cnt)
        ylabel = "log(Count)"
    else:
        y = cnt
        ylabel = "Count"
    #log_deg = np.log10(np.array(deg) + 1)

    ax = fig.add_subplot(2, 2, role+1)
    ax.set_title(f"Role={role}\nCount={sum(cnt)}, Avg={m:.1f}, Stdev={s:.1f}", fontsize=16)
    ax.plot(deg, y, ".", color='b')
    ax.set_ylabel(ylabel, fontsize=14)
    ax.set_xlabel("Degree", fontsize=14)

# fig.suptitle(f"Degree Histogram (k={k})", fontsize=20)
# fig.subplots_adjust(top=0.8)
fig.show()
fig.savefig(f"{save_path}/hist_allInOne.png")
plt.close(fig)

