from neo4j import GraphDatabase

# https://stackoverflow.com/questions/56710065

# prepare input list as a list of dictionary
data = [{"UserId": str(dic_node_decode[i]),
         "Role": str(roles[i])} for i in range(n)]

driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "team6"))

query = """
    UNWIND $list AS roleList 
    MATCH (u:users {UserId: toInteger(roleList.UserId)}) 
    SET u.Role = toInteger(roleList.Role);
"""

with driver.session() as session:
    with session.begin_transaction() as tx:
        tx.run(query, list=data)

