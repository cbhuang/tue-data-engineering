// put data into 
// ~/.config/Neo4j Desktop/Application/neo4jDatabases/database-5f2fed5e-7c2d-466a-a9f5-87c024371704/installation-3.5.5/import/

// users
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///data/users.csv" AS row
CREATE (:users {
    UserId: toInteger(row.Id),
    Reputation: toInteger(row.Reputation),
    CreationDate: row.CreationDate,
    DisplayName: row.DisplayName,
    LastAccessDate: row.LastAccessDate,
    WebsiteUrl: row.WebsiteUrl,
    Location: row.Location,
    AboutMe: row.AboutMe,
    Views: toInteger(row.Views),
    UpVotes: toInteger(row.UpVotes),
    DownVotes: toInteger(row.DownVotes),
    AccountId: toInteger(row.AccountId),
    Age: row.Age,
    ProfileImageUrl: row.ProfileImageUrl
});  // 40,325

CREATE CONSTRAINT ON (u:users) ASSERT u.UserId IS UNIQUE;


// posts
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///data/posts.csv" AS row
CREATE (:posts {
    PostId: toInteger(row.Id),
    PostTypeId: toInteger(row.PostTypeId),
    AcceptedAnswerId: toInteger(row.AcceptedAnswerId),
    CreaionDate: row.CreaionDate,
    Score: row.Score,
    ViewCount: row.ViewCount,
    Body: row.Body,
    OwnerUserId: toInteger(row.OwnerUserId),
    LasActivityDate: row.LasActivityDate,
    Title: row.Title,
    Tags: row.Tags,
    AnswerCount: toInteger(row.AnswerCount),
    CommentCount: toInteger(row.CommentCount),
    FavoriteCount: toInteger(row.FavoriteCount),
    LastEditorUserId: toInteger(row.LastEditorUserId),
    LastEditDate: row.LastEditDate,
    CommunityOwnedDate: row.CommunityOwnedDate,
    ParentId: toInteger(row.ParentId),
    ClosedDate: row.ClosedDate,
    OwnerDisplayName: row.OwnerDisplayName,
    LastEditorDisplayName: row.LastEditorDisplayName
});  //91,976

CREATE CONSTRAINT ON (u:posts) ASSERT u.PostId IS UNIQUE;
CREATE INDEX ON :posts(OwnerUserId);

// comments
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///data/comments.csv" AS row
CREATE (:comments {
    CommentId: toInteger(row.Id),
    PostId: toInteger(row.PostId),
    Score: toInteger(row.Score),
    Text: row.Text,
    CreationDate: row.CreationDate,
    UserId: toInteger(row.UserId),
    UserDisplayName: row.UserDisplayName
}); //174,305

CREATE CONSTRAINT ON (u:comments) ASSERT u.CommentId IS UNIQUE;


// tags
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///data/tags.csv" AS row
CREATE (:tags {
    TagId: toInteger(row.Id),
    TagName: row.TagName,
    Count: toInteger(row.Count),
    ExcerptPostId: toInteger(row.ExcerptPostId),
    WikiPostId: toInteger(row.WikiPostId)
}); //1,032

CREATE CONSTRAINT ON (u:tags) ASSERT u.TagId IS UNIQUE;


// votes
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///data/votes.csv" AS row
CREATE (:votes {
    VoteId: toInteger(row.Id),
    PostId: toInteger(row.PostId),
    VoteTypeId: toInteger(row.VoteTypeId),
    CreationDate: row.CreationDate,
    UserId: toInteger(row.UserId),
    BountyAmount: row.BountyAmount
});  // 328,064

CREATE CONSTRAINT ON (u:votes) ASSERT u.VoteId IS UNIQUE;


// badges
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///data/badges.csv" AS row
CREATE (:badges {
    BadgeId: toInteger(row.Id),
    UserId: toInteger(row.UserId),
    Name: row.Name,
    Date: row.Date
});  //79,851

CREATE CONSTRAINT ON (u:badges) ASSERT u.BadgeId IS UNIQUE;


// postLinks (not used)
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///data/postLinks.csv" AS row
CREATE (:postLinks {
    PostLinkId: toInteger(row.Id),
    CreationDate: row.CreationDate,
    PostId: toInteger(row.PostId),
    RelatedPostId: toInteger(row.RelatedPostId),
    LinkTypeId: toInteger(row.LinkTypeId)
});  //11,102

CREATE CONSTRAINT ON (u:postLinks) ASSERT u.PostLinkId IS UNIQUE;


// postHistory (not used)
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///data/postHistory.csv" AS row
CREATE (:postHistory {
    PostLinkId: toInteger(row.Id),
    CreationDate: row.CreationDate,
    PostId: toInteger(row.PostId),
    RelatedPostId: toInteger(row.RelatedPostId),
    LinkTypeId: toInteger(row.LinkTypeId)
});  //303,187

CREATE CONSTRAINT ON (u:postHistory) ASSERT u.PostHistoryId IS UNIQUE;
