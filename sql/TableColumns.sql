select
    table_name
    , column_name
--     , data_type
--     , column_type
--     , character_maximum_length
--     , ordinal_position
from information_schema.columns
where table_schema = "stackexchange";
