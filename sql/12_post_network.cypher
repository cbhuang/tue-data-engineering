//
// Construct network(s) of users
//
// WARNING: Do not run directly! 
// Usage: Please paste the necessary sections into neo4j!

/* Connect questions by the same answering user */
// Idea: Answered by a same user ~= From the same "skill set"
// CAUTION: OUT_OF_MEMORY_ERROR! must split up

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where 1 <= q1.PostId < 4000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r);  // Created 461056 relationships, completed after 163413 ms.

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where 4000 <= q1.PostId < 8000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r);  // Created 394768 relationships, completed after 83167 ms.

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where 8000 <= q1.PostId < 12000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r);  // Created 334626 relationships, completed after 65054 ms.

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where 12000 <= q1.PostId < 16000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r);  // Completed after 28565 ms.

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where 16000 <= q1.PostId < 20000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r); 

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where 20000 <= q1.PostId < 25000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r); 

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where 25000 <= q1.PostId < 30000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r); 

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where 30000 <= q1.PostId < 35000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r); 

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where 35000 <= q1.PostId < 40000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r); 

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where 40000 <= q1.PostId < 45000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r); 

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where 45000 <= q1.PostId < 50000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r); 

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where 50000 <= q1.PostId < 60000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r); 

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where 60000 <= q1.PostId < 70000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r); 

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where 70000 <= q1.PostId < 85000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r); 

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where 85000 <= q1.PostId < 100000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r); 

MATCH (q1:questions)<-[:BELONGS_TO]-(a1:answers)<-[:OWNS]-(u1:users)-[:OWNS]->(a2:answers)-[:BELONGS_TO]->(q2:questions)
    where q1.PostId >= 100000 and q1.PostId < q2.PostId
MERGE (q1)-[r:SAME_ANSWERER]->(q2)
RETURN count(r); 

// debug
// count
MATCH (q1:questions)-[r:SAME_ANSWERER]->(q2:questions) 
    WHERE q1.PostId < q2.PostId
return count(r); // 6,049,272 = 12,098,544 / 2

// visualize
MATCH (q1)-[r:SAME_ANSWERER]-(q2) 
return q1, r, q2 
limit 100;

// undo
match ()-[r:SAME_ANSWERER]-() delete r;


/* Connect questions by tags (depr) */
// CAUTION: O(n^2), cost hours!!

MATCH (q1:posts {PostTypeId: 1})-[:TAGGED]->(tag:tags)<-[:TAGGED]-(q2:posts {PostTypeId: 1})
    WHERE q1.PostId > q2.PostId
MERGE (q1)-[r:SAME_TAG]-(q2)
RETURN count(r);

// visualize
MATCH (q1)-[r:SAME_TAG]-(q2)
return q1, r, q2
limit 100;


/* Connect questions by a subset of tags (exploratory) */
// tagname: database, sql, c++

MATCH (q1:posts {PostTypeId: 1})-[:TAGGED]->(tag:tags {TagName: "database"})<-[:TAGGED]-(q2:posts {PostTypeId: 1})
    WHERE q1.PostId > q2.PostId
MERGE (q1)-[r:TMP_SAME_TAG]-(q2)
RETURN count(r);  // 55

MATCH (q1:posts {PostTypeId: 1})-[:TAGGED]->(tag:tags {TagName: "sql"})<-[:TAGGED]-(q2:posts {PostTypeId: 1})
    WHERE q1.PostId > q2.PostId
MERGE (q1)-[r:TMP_SAME_TAG]-(q2)
RETURN count(r);  // 105

MATCH (q1:posts {PostTypeId: 1})-[:TAGGED]->(tag:tags {TagName: "c++"})<-[:TAGGED]-(q2:posts {PostTypeId: 1})
    WHERE q1.PostId > q2.PostId
MERGE (q1)-[r:TMP_SAME_TAG]-(q2)
RETURN count(r);  // 210

// visualize
MATCH (q1)-[r:TMP_SAME_TAG]-(q2)
return q1, r, q2;
// expand on some nodes will discover a common tag "dataset"

// revert 
MATCH ()-[r:TMP_SAME_TAG]-() DELETE r;
