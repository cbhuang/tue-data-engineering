/*
Split questions and answers for future convenience
*/

/* questions */
match (n:posts {PostTypeId: 1})
set n :questions
return count(n);  // 42,921
// Speed up query
CREATE CONSTRAINT ON (q:questions) ASSERT q.PostId IS UNIQUE;
CREATE INDEX ON :questions(OwnerUserId);

// revert changes
// match (n:questions)
// remove n:questions
// return count(n);

/* answers */
match (n:posts {PostTypeId: 2})
set n :answers
return count(n);  // 47,765
// Speed up query
CREATE CONSTRAINT ON (q:answers) ASSERT q.PostId IS UNIQUE;
CREATE INDEX ON :answers(OwnerUserId);
