//
//  Create relations in Neo4j database after import
// 

// TODO: postLink, postHistory

/* user -> post */
MATCH (user:users), (post:posts)
    WHERE user.UserId = post.OwnerUserId
MERGE (user)-[owns:OWNS]->(post)
RETURN count(owns);  // 90,584

// revert changes
//MATCH ()-[r:OWNS]-() 
//DELETE r

// validate (exclude community id)
//MATCH (u:users)-[owns:OWNS]->(p:posts) WHERE u.UserId <> -1
//RETURN p, owns, u limit 20;

/* user -> comment */
MATCH (user:users), (comment:comments)
    WHERE user.UserId = comment.UserId
MERGE (user)-[owns:OWNS]->(comment)
RETURN count(owns);  // 171,470

/* post -> comment */
MATCH (post:posts), (comment:comments)
    WHERE post.PostId = comment.PostId
MERGE (post)-[owns:OWNS]->(comment)
RETURN count(owns);  // 174,305

/* user -> badge */
MATCH (user:users), (badge:badges)
    WHERE user.UserId = badge.UserId
MERGE (user)-[has:HAS]->(badge)
RETURN count(has);  // 79,851

/* user -> vote */
MATCH (user:users), (vote:votes)
    WHERE user.UserId = vote.UserId
MERGE (user)-[owns:OWNS]->(vote)
RETURN count(owns);  // 34,789

/* post -> vote */
MATCH (post:posts), (vote:votes)
    WHERE post.PostId = vote.PostId
MERGE (post)-[owns:OWNS]->(vote)
RETURN count(owns);  // 328,064

/* answer -> question  */
MATCH (a:posts), (q:posts)
    WHERE q.PostId = a.ParentId
MERGE (a)-[r:BELONGS_TO]->(q)
RETURN count(r);  // 47,755

/* post -> tag */
MATCH (post:posts), (tag:tags)
    WHERE post.Tags CONTAINS toString("<" + tag.TagName + ">")
MERGE (post)-[tagged:TAGGED]->(tag)
RETURN count(tagged);  // 117,656 ; very slow (3m25s)! 
