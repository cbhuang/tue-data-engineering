create database if not exists preprocessed;
use preprocessed;

/***** Preprocession *****/

/* 1. comment in post */
-- participation in comment is regarded as participation in the post
create temporary table if not exists tmp_comment as
select distinct
    postId, userId
from stackexchange.comments
where userId is not null;  -- 111,434 rows

/* 2. Questions */
drop temporary table if exists tmp_question;
create temporary table tmp_question as
select 
	id as postId,
    ownerUserId as userId
from stackexchange.posts
where parentId is null 
      and ownerUserId is not null; -- 43,484 rows

/* 3. Answers to Questions (take Questions) */
drop temporary table if exists tmp_answer;
create temporary table tmp_answer as
select distinct
    Q.Id as postId,
	A.ownerUserId as userId
from stackexchange.posts as Q
inner join stackexchange.posts as A
	on Q.Id = A.parentId 
       and A.ownerUserId is not null
       and Q.ownerUserId is not null; -- 45,781 rows

/***** I. User Network *****/

/* 1. Bipartite network of user-posts */

drop temporary table if exists tmp_userPostComment;
create temporary table tmp_userPostComment as
select postId, userId from tmp_comment
union
select postId, userId from tmp_question
union
select postId, userId from tmp_answer; -- 178,294 rows

-- write to disk
create table userPostComment as
select * 
from tmp_userPostComment
order by userId, postId;

-- cleanup
drop temporary table tmp_userPostComment;

-- create index for merging efficiency
create index if not exists idx_post on userPostComment (postId);
create index if not exists idx_user on userPostComment (userId);

/* 2. Uni-partite network of user */

-- Edges with duplicates
drop table if exists userEdgeByPostComment;
create table userEdgeByPostComment as
select 
	A.userId as node1,
    B.userId as node2
from userPostComment as A
inner join userPostComment as B
	on A.postId = B.postId
order by node1, node2;  -- 618,720 rows, 3.2s;

-- Edges without duplicates
drop table if exists userDistinctEdgeByPostComment;
create table userDistinctEdgeByPostComment as
select distinct 
    node1, node2 
from userEdgeByPostComment
order by node1, node2;  -- 282,070 rows, 1.618s;


/***** II. Posts Network *****/

/* 1. Bipartite network of post-user (no comment) */

drop temporary table if exists tmp_postUser;
create temporary table tmp_postUser as
select postId, userId from tmp_question
union
select postId, userId from tmp_answer; -- 87,870 rows

-- write to disk
create table postUser as
select * 
from tmp_postUser
order by userId, postId;

-- cleanup
drop temporary table tmp_postUser;

-- create index for merging efficiency
create index if not exists idx_post on postUser (postId);
create index if not exists idx_user on postUser (userId);

/* 2. Uni-partite network of posts (no comment) */

-- Edges with duplicates
drop table if exists postEdgeByUser;
create table postEdgeByUser as
select 
	A.postId as node1,
    B.postId as node2
from postUser as A
inner join postUser as B
	on A.userId = B.userId
order by node1, node2;  -- 14,149,226 rows, 104s;

-- Edges without duplicates
drop table if exists postDistinctEdgeByUser;
create table postDistinctEdgeByUser as
select distinct 
    node1, node2 
from postEdgeByUser
order by node1, node2;  -- 14,012,156 rows, 160s;
