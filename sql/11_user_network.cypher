//
// Construct network(s) of users
//
// WARNING: Do not run directly! 
// Usage: Please paste the necessary sections into neo4j!

/* Connect users by posts */
// participation of the SAME_THREAD = 1. + 2.

// 1. user1 answers user2 (directed, including self-answering)
MATCH (user1:users)-[:OWNS]->(a:answers)-[:BELONGS_TO]->(q:questions)<-[:OWNS]-(user2:users)
MERGE (user1)-[r:ANSWERS {QuestionId: q.PostId}]->(user2)
RETURN count(r);  // 46,138; 44,696(where user1.UserId <> user2.UserId)

// 2. user1 and user2 answers the same question (undirected)
MATCH (user1:users)-[:OWNS]->(a1:answers)-[:BELONGS_TO]->(q:questions)<-[:BELONGS_TO]-(a2:answers)<-[:OWNS]-(user2:users)
    WHERE user1.UserId < user2.UserId
MERGE (user1)-[r:SAME_QUESTION {QuestionId: q.PostId}]->(user2)  // in fact undirected
RETURN count(r);  // 46,093(directed) = 92,186/2

// SAME_THREAD (undirected, no multiplicity)
MATCH (u1:users)-[:SAME_QUESTION|:ANSWERS]-(u2:users)
    WHERE u1.UserId <= u2.UserId
MERGE (u1)-[r:SAME_THREAD]->(u2)  // distinct
return count(r); // 84,858 processed, but only 68,920 left (seen later)

// debug below
// count
MATCH (u1:users)-[r:SAME_THREAD]->(u2:users)
return count(r); // 68,920 edges (already distinct)

// UserId example
MATCH (u1:users)-[r:SAME_THREAD]-(u2:users)
    where u1.UserId in [5, 22] and u2.UserId in [5, 22]
return u1.UserId, u2.UserId, r.QuestionId, type(r)
order by u1.UserId, u2.UserId, type(r), r.QuestionId;

// visualize
MATCH (user1:users)-[r:SAME_THREAD]-(user2:users) 
return user1, r, user2
limit 50;

// undo
MATCH (:users)-[r:SAME_QUESTION|:ANSWERS|:SAME_THREAD]-(:users) delete r;
