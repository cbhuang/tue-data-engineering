"""
Local code prototype: construction of social network

:author: Bill Huang
"""
import networkx as nx
import os
import pandas as pd
import numpy as np
import pymysql
from settings import *


"""
Info: Raw Data
==================
"""
conn = pymysql.connect(host=HOST,
                       user=USER,
                       password=PASSWD,
                       db=DB_RAW,
                       #charset='utf8mb4',
                       cursorclass=pymysql.cursors.DictCursor)
cur = conn.cursor()

# list of size of tables
sql = """select TABLE_NAME, TABLE_ROWS
         from INFORMATION_SCHEMA.TABLES
         where TABLE_SCHEMA = "stackexchange";"""
cur.execute(sql)
fetched = cur.fetchall()
df_rows = pd.DataFrame.from_dict(fetched)
df_rows.to_csv("result/db_tables_rows.csv")
print(df_rows)

cur.close()
conn.close()


"""
Networks
====================
"""

# preprocessed database
conn = pymysql.connect(host=HOST,
                       user=USER,
                       password=PASSWD,
                       db=DB_PRE,
                       cursorclass=pymysql.cursors.DictCursor)
cur = conn.cursor()  
# don't forget to close both!

"""
1. User, connected by Posts and Comments
-------------------------------------------

How to define with user participation on Posts:
    - (1) Answers to the Post
    - (2) Comments on the Post
    - (3) Comments on any Answer to the Post
"""


# data retrieval
cur.execute("select * from userDistinctEdgeByPostComment;")
fetched = cur.fetchall()
df_users = pd.DataFrame(fetched)
print(df_users.head())

# construct graph
G = nx.from_pandas_edgelist(df_users, source="node1", target="node2")

# Print summary info
print(nx.info(G))
# Number of nodes: 22558
# Number of edges: 152314
# Average degree:  13.5042

# Export for dedicated software such as
nx.write_graphml(G, "result/userDistinctEdgeByPostComment.graphml")


"""
2. Posts, connected by User (w/o Comments)
-----------------------------------------------------
"""

# data retrieval
cur.execute("select * from postDistinctEdgeByUser;")
fetched = cur.fetchall()
df_posts = pd.DataFrame(fetched)
print(df_posts.head())

# WARNING: Too big to do on PC!
# construct graph
#G = nx.from_pandas_edgelist(df_posts, source="node1", target="node2")
# Print summary info
#print(nx.info(G))

# Number of nodes: 43484
# Number of edges: 14012156
# Average degree:  644.474


"""
3. Comments (TODO: explore later)
----------------------------------
"""



# close connection
cur.close()
conn.close()
